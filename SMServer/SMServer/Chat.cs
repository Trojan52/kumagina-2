﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;

namespace SMServer
{
    class Chat
    {
        private List<Socket> ChatUsers;

        public Chat()
        {
            ChatUsers = new List<Socket>();
        }

        public bool CheckUser(Socket user)
        {
            if(ChatUsers.Contains(user))
                return false;
            else
                return true;
        }

        public void AddUser(Socket user)
        {
            ChatUsers.Add(user);
            new Thread(ServeUser).Start(user);
        }

        public void SendToAllUsers(byte[] msg)
        {
            foreach (Socket client in ChatUsers)
            {
                client.Send(msg);
            }
        }
       
        private void ServeUser(object sock)
        {
            
            Socket handler = (Socket)sock;

            try
            {

                
                while (true)
                {
                    string data = null;
                    byte[] bytes = new byte[1024];
                    int bytesRec = handler.Receive(bytes);
                    if (bytesRec == 0)
                    {
                        RemoveUser(handler);
                        return;
                    }
                    data += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                    if (data.IndexOf("stop") > -1)
                    {
                        Console.WriteLine("Сервер завершил соединение с клиентом.");
                        RemoveUser(handler);
                        return;
                    }

                    Console.Write("Полученный текст: " + data + "\n\n");
                    

                    byte[] msg = Encoding.UTF8.GetBytes(data);
                    SendToAllUsers(msg);
                   
                }

            }
            catch (Exception ex)
            {
                
                RemoveUser(handler);
            }

           
        }

        private void RemoveUser(Socket sock)
        {
            Console.WriteLine("Соединение разорвано");

            if (ChatUsers.Contains(sock))
            {
                ChatUsers.Remove(sock);
                sock.Shutdown(SocketShutdown.Both);
                sock.Close();
            }
            SendToAllUsers(Encoding.UTF8.GetBytes("SERVER > Пользователь отключился!"));
        }

    }
}
