﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
namespace SMServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Chat chat = new Chat();
            IPAddress ipAddr = null;
            IPHostEntry ipHost = Dns.GetHostEntry(IPAddress.Parse("127.0.0.1"));
            foreach (IPAddress ip in ipHost.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    ipAddr = ip;
                    break;
                }
            }
            if (ipAddr == null) { Console.WriteLine("Couldn't init server..."); return; }
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11000);
            Console.WriteLine("Ожидание подключения к {0}:{1}", ipAddr.ToString(), 11000);

            Socket sListener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                sListener.Bind(ipEndPoint);
                sListener.Listen(10);


                while (true)
                {
                    Socket user = sListener.Accept();
                    if (chat.CheckUser(user))
                    {
                        chat.AddUser(user);
                        Console.WriteLine("Соединение установлено");
                        chat.SendToAllUsers(Encoding.UTF8.GetBytes("SERVER > Подключен новый пользователь!"));
                    }
                    
                    
                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
        }

        

    }
}
