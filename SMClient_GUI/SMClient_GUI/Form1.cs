﻿using System;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace SMClient_GUI
{
    public partial class frmChat : Form
    {
        Socket UserSocket = null;
        Thread ReceiveThread = null;
        string UserName = "";
        public frmChat()
        {
            InitializeComponent();
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            if (txtName.Text.Trim().Length > 0)
                btnConnect.Enabled = true;
            else
                btnConnect.Enabled = false;
        }

        private void frmChat_Shown(object sender, EventArgs e)
        {
            IPAddress ipAddr = null;
            IPHostEntry ipHost = Dns.GetHostEntry(IPAddress.Parse("127.0.0.1"));
            foreach (IPAddress ipA in ipHost.AddressList)
            {
                if (ipA.AddressFamily == AddressFamily.InterNetwork)
                {
                      ipAddr = ipA;
                      break;
                }
            }
            txtIP.Text = ipAddr.ToString();
            
        }

        private void ReceiveMessages()
        {
            while (true)
            {
                if (UserSocket.Available > 0)
                {
                    byte[] bytes = new byte[1024];
                    int bytesRec = UserSocket.Receive(bytes);
                    string reply = Encoding.UTF8.GetString(bytes, 0, bytesRec);
                    if (InvokeRequired)
                    {
                        Invoke((Action<string>)txtChat.AppendText,reply+'\n');
                    }
                    else
                    {
                        txtChat.AppendText(reply + '\n');
                    }
                }
                Thread.Sleep(100);
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            IPAddress ipAddr = null;
            UserName = txtName.Text;
            if (txtIP.Text.Trim().Length == 0)
            {
                IPHostEntry ipHost = Dns.GetHostEntry(IPAddress.Parse("127.0.0.1"));
                foreach (IPAddress ipA in ipHost.AddressList)
                {
                    if (ipA.AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddr = ipA;
                        break;
                    }
                }
                txtIP.Text = ipAddr.ToString();
                
            }
            else
            {
                ipAddr = IPAddress.Parse(txtIP.Text);
            }
            try
            {
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11000);
                UserSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                UserSocket.Connect(ipEndPoint);
                txtIP.Enabled = false;
                txtName.Enabled = false;
                btnConnect.Enabled = false;
                ReceiveThread = new Thread(ReceiveMessages);
                ReceiveThread.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Что-то сломалось! Звоните фиксикам");
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            SendMessage();

        }

        private void SendMessage()
        {
            if (UserSocket == null)
                MessageBox.Show("Вы не подключены к серверу!");
            else
            {
                try
                {
                    byte[] msg = Encoding.UTF8.GetBytes(UserName + " > " + txtMessage.Text);
                    UserSocket.Send(msg);
                    txtMessage.Clear();
                    btnSend.Enabled = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Что-то сломалось при отправке сообщения!\nЗвоните фиксикам");
                }
            }
        }

        private void txtMessage_TextChanged(object sender, EventArgs e)
        {
            if (txtMessage.Text.Trim().Length > 0 && UserSocket != null)
                btnSend.Enabled = true;
            else
                btnSend.Enabled = false;
        }

        
        private void frmChat_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (UserSocket != null)
                {
                    UserSocket.Shutdown(SocketShutdown.Both);
                    UserSocket.Close();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {

                if (ReceiveThread != null)
                {
                    ReceiveThread.Abort();
                    ReceiveThread.Join();
                }
            }
        }
                
    }
}
